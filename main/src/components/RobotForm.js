import React, { Component } from "react";

class RobotForm extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      type: "",
      mass: "",
    };
    this.submit = this.submit.bind(this);
  }

  submit(e) {
    e.preventDefault();
    this.setState({
      name: "",
      type: "",
      mass: "",
    });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.submit}>
          <input
            id="name"
            value={this.state.name}
            onChange={(event) => this.setState({ name: event.target.value })}
            placeholder="Robot name"
            required
          />
          <input
            id="type"
            value={this.state.type}
            onChange={(event) => this.setState({ type: event.target.value })}
            placeholder="Robot type"
            required
          />
          <input
            id="mass"
            value={this.state.mass}
            onChange={(event) => this.setState({ mass: event.target.value })}
            placeholder="Robot mass"
            required
          />
          <button type="submit" onClick={() => this.props.onAdd(this.state)} value="add">
            add
          </button>
        </form>
      </div>
    );
  }
}

export default RobotForm;
